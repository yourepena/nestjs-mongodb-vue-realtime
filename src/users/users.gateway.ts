import { SubscribeMessage, WebSocketGateway, WebSocketServer, OnGatewayConnection } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { User } from './users/users.model';
import { ModelType } from 'typegoose';
import { InjectModel } from '@nestjs/mongoose';

@WebSocketGateway({ namespace: '/users' })
export class UsersGateway implements OnGatewayConnection {
  
  constructor(@InjectModel(User.modelName) private readonly _userModel: ModelType<User>) { }

  @WebSocketServer() wss: Server;

  handleConnection(client: any, ...args: any[]) {
    this._userModel.watch().on("change", change => {
      console.log(`[SERVER_CHANGE_STREAM] User:`);
      this.wss.emit("changeData", change.fullDocument);
    });
  }

  change(changeData: User): void {
    this.wss.emit("changeData", changeData);
  }

}
