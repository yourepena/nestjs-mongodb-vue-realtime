import { Module } from '@nestjs/common';
import { UsersService } from './users/users.service';
import { UsersController } from './users/users.controller';
import { UsersGateway } from './users.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { User } from './users/users.model';

@Module({
  imports: [MongooseModule.forFeature([{ name: User.modelName, schema: User.model.schema }])],
  providers: [UsersService, UsersGateway],
  controllers: [UsersController]
})
export class UsersModule {}
