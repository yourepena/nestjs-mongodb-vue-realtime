import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { User } from './users.model';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

    constructor(private userService: UsersService) {}

    @Post()
    @HttpCode(200)
    sendAlertToAll(@Body() dto: User) {
       this.userService.register(dto);
        return dto;
    }

}


 