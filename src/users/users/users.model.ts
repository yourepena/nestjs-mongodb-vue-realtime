import { SchemaOptions } from 'mongoose';
import { ModelType, pre, prop, Typegoose } from 'typegoose';


@pre<User>('findOneAndUpdate', function (next) {
    this._update.updatedAt = new Date(Date.now());
    next();
})
export class User extends Typegoose {
    @prop()
    name: string;

    static get model(): ModelType<User> {
        return new User().getModelForClass(User, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const schemaOptions: SchemaOptions = {
    toJSON: {
        virtuals: true,
        getters: true,
    },
};


export const UserModel = new User().getModelForClass(User, { schemaOptions });
