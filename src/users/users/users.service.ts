import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserModel } from './users.model';
import { ModelType } from 'typegoose';
import { UsersGateway } from '../users.gateway';

@Injectable()
export class UsersService {

    constructor(@InjectModel(User.modelName) private readonly _userModel: ModelType<User>,
        private readonly usersGateway: UsersGateway
    ) { }

    async register(vm: User) {
        const { name } = vm;
        const newTarefa = new UserModel();
        newTarefa.name = name.trim().toLowerCase();
        try {
            const result = await this._userModel.create(newTarefa);
            const userResult = result.toJSON() as User;
            this.usersGateway.change(userResult);
            return userResult
        } catch (e) {
            console.error("Ocorreu um erro tentando salvar o usuario: ", e);
        }
    }

}
