import { Module } from '@nestjs/common';
import { ChatGateway } from './chat/chat.gateway';
import { AlertGateway } from './alert/alert.gateway';
import { AlertController } from './alert/alert.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';



@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/UserDB?replicaSet=rs0', {
    retryDelay: 500,
    retryAttempts: 3,
    useNewUrlParser: true,
}), UsersModule],
  controllers: [AlertController],
  providers: [ChatGateway, AlertGateway],
})
export class AppModule {}
